<iframe src="https://onedrive.live.com/embed?cid=FB9A18AE325D3EFB&amp;resid=FB9A18AE325D3EFB%215303&amp;authkey=AD1so520UpPt9nQ&amp;em=2&amp;wdAr=1.7777777777777777" width="610px" height="367px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Special considerations when dealing with social data
In this section of the tutorial, we focus on how to design a causal study in a social network setting.  We will provide examples of some well-designed studies and use them to review some of the more complex issues that arise in social network settings.  While we will not be able to go into depth on solutions, we will teach attendees how to recognize these challenges and provide pointers to current research approaches.
    
The challenges of social network and data settings include:
* Common confounders in social data analyses.
* Challenges in high-dimensional data analyses.
* Interference due to network effects.
* Context-dependence of measured effects

    


