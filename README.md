![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

#Tutorial on Causal Inference and Counterfactual Reasoning

[Emre Kiciman](http://kiciman.org) ([@emrek](https://twitter.com/emrek)), [Amit Sharma](http://www.amitsharma.in) ([@amt_shrma](https://twitter.com/amt_shrma))




AAAI ICWSM 2018
International Conference on Web and Social Media 2018, Stanford, CA.

http://causalinference.gitlab.io/icwsm-tutorial

http://github.com/Microsoft/dowhy

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!--**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

 [Summary](#summary)-->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Summary
A critical goal in social science research is to infer causal mechanisms of behavior. 
Digital systems have provided new ways of collecting large-scale data about social questions, 
but also present new challenges for causal inference from this data. 
This tutorial will introduce participants to concepts in causal inference and counterfactual reasoning, 
drawing from a broad literature on the topic from statistics, social sciences and machine learning. 

We first motivate the use of causal inference with social and online data through examples 
in domains such as online social networks, health, education and governance. 
To tackle such questions, we will introduce the key ingredient that causal analysis 
depends on---counterfactual reasoning---and describe the two most popular frameworks 
based on Bayesian graphical models and potential outcomes. Based on this, we 
cover a range of methods suitable for doing causal inference in social data, 
including randomized experiments, observational methods like matching and stratification, 
and natural experiment-based methods such as instrumental variables and regression discontinuity. 
We also focus on best practices for evaluation and validation of causal inference techniques, 
drawing from our experience in working with social datasets. 
Throughout, the emphasis is on special considerations with social data, 
such as dealing with high-dimensionality or an underlying social network.

