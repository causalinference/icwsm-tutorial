<iframe src="https://onedrive.live.com/embed?cid=FB9A18AE325D3EFB&amp;resid=FB9A18AE325D3EFB%215302&amp;authkey=ABieo34OSbjEAPc&amp;em=2&amp;wdAr=1.7777777777777777" width="610px" height="367px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Broader landscape in causal reasoning
We close our tutorial by providing awareness of the landscape of open research and challenges in causal reasoning beyond inference methods.
    
The broader landscape review will touch on the following topics:
* Discovery of causal relationships from data.
* Estimating heterogenous treatment effects
* Machine learning, representations and causal inference.
* Automating causal inference
* Necessary and sufficient causes for events.
    

