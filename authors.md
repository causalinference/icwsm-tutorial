<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# About the Authors

## Amit Sharma, Microsoft Research India 
http://www.amitsharma.in

Amit Sharma is a researcher at Microsoft Research India. His research focuses on understanding the underlying mechanisms that shape people's activities as they interact with algorithmic systems, with an emphasis on the effect of recommendation systems and social influence. More generally, his work contributes to methods for causal inference from large-scale data, combining principles from Bayesian graphical models, data mining and machine learning. He completed his Ph.D. in computer science at Cornell University. He has received a Best Paper Honorable Mention Award at the 2016 ACM Conference on Computer Supported Cooperative Work and Social Computing (CSCW), the 2012 Yahoo! Key Scientific Challenges Award and the 2009 Honda Young Engineer and Scientist Award.

Amit co-organized the first workshop on Fairness, Accountability and Transparency (FATREC) at ACM Conference on Recommender Systems 2017, and was the lead organizer for a Microsoft-USC AI for Social Good workshop at Bangalore India. In addition, he is on the Steering Committee for the FAT\* conference, has served on many conference and workshop PC committees, and is an active part of the ICWSM, WWW and CSCW communities.


## Emre Kiciman, Microsoft Research AI 
http://kiciman.org/

Emre Kiciman (http://research.microsoft.com/~emrek) is a Principal Researcher at Microsoft Research AI. His current research focuses on causal analysis of large-scale social media timelines, and he is broadly interested in using social data to support individuals and policy-makers. Emre’s past research includes new entity linking methods for social media and the web, deployed in the Bing search engine; and foundational work on applying machine learning to fault management in large-scale internet services, now an industry standard practice. Emre has served as the steering committee chair of the AAAI International Conference on Web and Social Media (ICWSM 2013-2017), general chair and program chair of major conferences and as SPC or PC member of several data mining and machine learning conferences, including KDD, WWW, WSDM, and IJCAI. Emre received his Ph.D. and M.S. from Stanford University, and his B.S. in Electrical Engineering and Computer Science from U.C. Berkeley.


